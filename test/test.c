
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>


struct test
{
    /* data */
    unsigned a:1;
};

int uri_has_param(char *str){
    int i = 0;
    char s[] = "?";
    for(i=0;str[i];i++){
        if(str[i]==s[0]){
            return i;
        }
    }
    return 0;
}


char *unsafe_headers[] = {"Cookie"};
int len = (int)(sizeof(unsafe_headers)/sizeof(unsafe_headers[0]));
int has_match_header(char *header){
    int i;
    for(i=0;i<len;i++){
        if(unsafe_headers[i] == header){
            return 1;
        }
    }
    return 0;
}

int fun(char *str,char *find)
{
    int i,j,n=0;
    for(i=0;str[i];i++)
    {
        if(str[i]==find[0])
            for(j=1;;j++)
            {
                if(find[j]==0)return i+1;//返回位置
                if(find[j]==str[i+j])n++;
                else break;           
            }          
    }
    return 0;//不存在返回0
}

struct 
{
    /* data */
    char* name;
} test;

/**判断str1是否以str2开头
 * 如果是返回1
 * 不是返回0
 * 出错返回-1
 * */
int is_begin_with(const char * str1,char *str2)
{
    if(str1 == NULL || str2 == NULL)
        return -1;
    int len1 = strlen(str1);
    int len2 = strlen(str2);
    if((len1 < len2) ||  (len1 == 0 || len2 == 0))
        return -1;
    char *p = str2;
    int i = 0;
    while(*p != '\0')
    {
        if(*p != str1[i])
            return 0;
        p++;
        i++;
    }
    return 1;
}

/**判断str1是否以str2结尾
 * 如果是返回1
 * 不是返回0
 * 出错返回-1
 * */
int is_end_with(const char *str1, char *str2)
{
    if(str1 == NULL || str2 == NULL)
        return -1;
    int len1 = strlen(str1);
    int len2 = strlen(str2);
    if((len1 < len2) ||  (len1 == 0 || len2 == 0))
        return -1;
    while(len2 >= 1)
    {
        if(str2[len2 - 1] != str1[len1 - 1])
            return 0;
        len2--;
        len1--;
    }
    return 1;
}

int is_static_uri(char *uri, int place){
    if(!place){
        place = strlen(uri);
    }
    char dest[200] = "";
    strncpy(dest, uri, place);

    printf("dest %s \n", dest);

    char *formates[] = {".js",".css",".png",".jpg",".jpeg"};

    int formates_len = sizeof(formates) / sizeof(formates[0]);

    int ret = 0;

    for(int i=0;i<formates_len;i++){
        ret = is_end_with(dest, formates[i]);
        if(ret){
            break;
        }
    }

    return ret;
}

int main(){
    // char *header_name = "Cookie";
    // if(has_match_header(header_name)){
    //     printf("has match");
    // }else{
    //     printf("no match");
    // }
    char *p = "test.css?a=1";
    int place = uri_has_param(p);


    int ret = is_static_uri(p, place);


    printf("ret %d \n", ret);

}